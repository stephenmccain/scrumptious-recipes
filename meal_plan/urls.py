from django.urls import path

from meal_plan.views import (
        MealPlanListView,
        MealPlanCreateView,
        MealPlanDeleteView,
        MealPlanUpdateView,
        MealPlanDetailView,
        )

urlpatterns = [
        path("meal_plan/", MealPlanListView.as_view(), name="meal_plan_list"),
        path("create", MealPlanCreateView.as_view(), name="meal_plan_create"),
        path("<int:pk>/", MealPlanDetailView.as_view(), name="meal_plan_detail"),
        path("<int:pk>edit/", MealPlanUpdateView.as_view(), name="meal_plan_update"),
        path("<int:pk>delete/", MealPlanDeleteView.as_view(), name="meal_plan_delete"),
]
