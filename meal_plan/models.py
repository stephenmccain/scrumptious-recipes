from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class MealPlan(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    recipes = models.ManyToManyField("recipes.Recipe", related_name="recipe")