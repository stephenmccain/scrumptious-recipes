from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin

from meal_plan.models import MealPlan

# Create your views here.

 
class MealPlanListView(ListView):
    model = MealPlan
    template_name = "mealplan/list.html"
    paginate_by = 2


class MealPlanDetailView(DetailView):
    model = MealPlan
    template_name = "mealplan/detail.html"


class MealPlanCreateView(CreateView):
    model = MealPlan
    template_name = "mealplan/new.html"
    fields = ["name", "description", "image"]
    success_url = reverse_lazy("meal_plan_list")

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("meal_plan_detail", pk=plan.id)


class MealPlanUpdateView(UpdateView):
    model = MealPlan
    template_name = "recipes/edit.html"
    fields = ["name", "author", "description", "image"]
    success_url = reverse_lazy("meal_plan_list")


class MealPlanDeleteView(DeleteView):
    model = MealPlan
    template_name = "mealplan/delete.html"
    success_url = reverse_lazy("mealplan_list")
